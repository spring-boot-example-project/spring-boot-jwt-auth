package vip.yazilim.worem.user;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IUserRepository extends CrudRepository<UserModel, Integer> {

    Optional<UserModel> findByUsername(String username);

    List<UserModel> findAll();
}
