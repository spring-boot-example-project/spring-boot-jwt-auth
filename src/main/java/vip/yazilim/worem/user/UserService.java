package vip.yazilim.worem.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private IUserRepository userRepository;

    PasswordEncoder passwordEncoder;

    public String save(UserModel userModel){
        passwordEncoder = new BCryptPasswordEncoder();
        String userEncodedPassword = passwordEncoder.encode(userModel.getPassword());
        userModel.setPassword(userEncodedPassword);
        userRepository.save(userModel);
        return "ok";
    }

    public Optional<UserModel> getUserByUsername(String username){
        return userRepository.findByUsername(username);
    }
}
