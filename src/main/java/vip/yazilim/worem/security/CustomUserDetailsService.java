package vip.yazilim.worem.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import vip.yazilim.worem.user.UserModel;
import vip.yazilim.worem.user.UserService;

import java.util.Optional;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<UserModel> userModelOptional = userService.getUserByUsername(username);
        userModelOptional.orElseThrow(()->new UsernameNotFoundException("User Not found for this name "+username));
        return new UserDetailsConf(userModelOptional.get());
    }
}