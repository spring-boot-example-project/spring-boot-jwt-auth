package vip.yazilim.worem.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import vip.yazilim.worem.security.jwt.JwtUtil;

import javax.servlet.http.HttpServletRequest;

@RestController
@Slf4j
public class HelloSpringSecurity {

    @Autowired
    JwtUtil jwtUtil;

    @GetMapping("/hello")
    public String helloUser(HttpServletRequest request) {
        String header = request.getHeader("Authorization");
        String token = header.split("Bearer ",2)[1];
        String userName = jwtUtil.extractUsername(token);
        return "Hello " + userName;
    }
}