package vip.yazilim.worem;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import vip.yazilim.worem.user.UserModel;
import vip.yazilim.worem.user.UserService;

@SpringBootApplication
public class WoRemApplication {

    public static void main(String[] args) {
        SpringApplication.run(WoRemApplication.class, args);
    }

    @Bean
    CommandLineRunner createInitialUsers(UserService userService){
        return new CommandLineRunner() {
            @Override
            public void run(String... args) throws Exception {
                UserModel user1 = new UserModel();
                user1.setUsername("anil");
                user1.setPassword("123456");
                userService.save(user1);
            }
        };
    }
}
